import Cardano from "../serialization-lib";
import { deserializeBounty, serializeBountyDatum, serializeTreasuryDatum } from "./datums";
import {
  assetsToValue,
  createTxOutput,
  createTxUnspentOutput,
  finalizeTx,
  initializeTx,
} from "../transaction";
import { fromBech32, fromHex, toHex } from "../../utils/converter";
import { contractScripts } from "./validator";
import { getAddressKeyHash } from "../../utils/factory";

// Project Instance Variables:
export const treasuryIssuerAddress = "addr1qy3xh5h4gjt6z69jqvrt04l0g2d82h4s6cf0xvu7t2nz95j6jnltpt22t0myhsuk9s6lyn74yxmlxakt27ylr9ykh30qz97lf8"
export const treasuryContractAddress = "addr1w96xgwa3hhfu7mfazsaz8jkg0kf3fpk9x5z6r50y2vf23dq7aa2s5"
export const bountyContractAddress = "addr1wy48f7uvzrv7agqz2prdchg3dv3ksleqn4gnvckhsdmzjzgp6f497"
export const accessPolicyID = "68ae22138b3c82c717713d850e5ee57c7de5de8591f5f13cd3a6cc67"
export const bountyTokenUnit = "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c3067696d62616c"

export const commitToBounty = async (bDatum, tDatum, { contributorAddress, utxosParam, bountySlug, accessTokenName, bLovelace, bGimbals, tUtxo, tLovelaceIn, tGimbalsIn }) => {
  try {
    const { txBuilder, datums, outputs } = initializeTx();
    const utxos = utxosParam.map((utxo) =>
      Cardano.Instance.TransactionUnspentOutput.from_bytes(fromHex(utxo))
    );

    // Convert Access Token Name to Hex
    const accessTokenHex = toHex(accessTokenName)
    console.log("Access Token:", accessTokenName, accessTokenHex)


    // do some quick arithmetic: the Treasury Contract output should contain all of the Lovelace and Gimbals that are NOT sent to the bounty.
    // (In order to unlock funds from the Treasury, The Treasury Validator will insist on meeting this condition.)
    const tLI = parseInt(tLovelaceIn)
    const tGI = parseInt(tGimbalsIn)
    const lovelaceToTreasury = tLI - bLovelace
    const gimbalsToTreasury = tGI - bGimbals

    console.log("bounty bound:", bLovelace, bGimbals, lovelaceToTreasury, gimbalsToTreasury)
    console.log(bountyContractAddress)

    const bountyDatum = serializeBountyDatum(bDatum);
    console.log("serialized bountyDatum", bountyDatum);
    console.log("and d", deserializeBounty(bountyDatum))

    const treasuryDatum = serializeTreasuryDatum(tDatum);
    datums.add(treasuryDatum);

    const dataForRedeemer = {
      issuer: getAddressKeyHash(treasuryIssuerAddress),
      contributor: getAddressKeyHash(contributorAddress),
      lovelaceAmount: `${bLovelace}`,
      tokenAmount: `${bGimbals}`,
      expirationTime: "1000000000"
    }

    console.log("dataForRedeemer", dataForRedeemer)

    // 4-17 -- why are inputs exhausted?

    // Output to Bounty Contract

    // TODO: The Unit string for the Access token must by dynamic
    outputs.add(
      createTxOutput(
        Cardano.Instance.Address.from_bech32(bountyContractAddress),
        assetsToValue([
          { unit: "lovelace", quantity: `${bLovelace}` },
          { unit: `${bountyTokenUnit}`, quantity: `${bGimbals}` },
          { unit: `${accessPolicyID}${accessTokenHex}`, quantity: "1"}
        ]),
        { datum: bountyDatum }
      )
    );

    outputs.add(
      createTxOutput(
        Cardano.Instance.Address.from_bech32(treasuryContractAddress),
        assetsToValue([
          { unit: "lovelace", quantity: `${lovelaceToTreasury}` },
          { unit: `${bountyTokenUnit}`, quantity: `${gimbalsToTreasury}` }
        ]),
        { datum: treasuryDatum }
      )
    );

    const treasuryUtxo = createTxUnspentOutput(Cardano.Instance.Address.from_bech32(treasuryContractAddress), tUtxo)

    const txHash = await finalizeTx({
      txBuilder,
      datums,
      utxos,
      outputs,
      changeAddress: fromBech32(contributorAddress),
      metadata: bountySlug,
      scriptUtxo: treasuryUtxo,
      bountyInfo: dataForRedeemer, // just changed
      plutusScripts: contractScripts(),
    });
    return {
      txHash,
    };
  } catch (error) {
    console.log(error, "commitToBounty");
    return {
      error
    }
  }
};