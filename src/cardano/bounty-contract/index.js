import Cardano from "../serialization-lib";
// we can use the same Datum builder that we used when unlocking the Treasury Contract
import { serializeBountyDatum } from "../treasury-contract/datums";
import {
  assetsToValue,
  createTxOutput,
  createTxUnspentOutput,
  finalizeTx,
  initializeTx,
} from "../transaction";
import { fromBech32, toHex, fromHex } from "../../utils/converter";
import { DISTRIBUTE, serializeBountyRedeemer } from "./redeemers";
import { contractScripts } from "./validator";
import { accessPolicyID, bountyContractAddress, bountyTokenUnit } from "../treasury-contract";


// --tx-in $ISSUERTXIN \ -- this is taken care of by our wallet
// --tx-in $BOUNTYTXIN \ -- TODO #1 below
// --tx-in-script-file bounty-play-testnet.plutus \ -- Is ready, needs to be added
// --tx-in-datum-file datum-play-test3.json \
// --tx-in-redeemer-file distribute.json \
// --tx-in-collateral $COLLATERAL \
// --tx-out $CONTRIBUTOR+"12000000 + 1 3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c.506c7574757350424c436f757273653031 + 51 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
// --change-address $SENDER \
// --required-signer /home/james/hd2/monday/payment.skey \
// --protocol-params-file protocol.json \
// --out-file unlock.raw

// To Do:
// 1. How is contract utxo passed to Treasury Tx? DONE
// 2. Add plutus contract to this transaction builder DONE
// 3. Build and pass the datum DONE
// 4. Brings us back to Redeemer todos (see notebook) --- in THIS FILE, the ACTION is inherent to the distributeBounty function
// 5. May need to explore required-signer?
// 6. Dynamically get the "unit" for any Access Token

export const distributeBounty = async (bUtxo, bDatum, { issuerAddress, contributorAddress, utxosParam, slug, accessTokenName, lovelace, gimbals }) => {
  try {
    const { txBuilder, datums, outputs } = initializeTx();
    const utxos = utxosParam.map((utxo) =>
      Cardano.Instance.TransactionUnspentOutput.from_bytes(fromHex(utxo))
    );

    // Convert Access Token Name to Hex
    const accessTokenHex = toHex(accessTokenName)

    const bountyDatum = serializeBountyDatum(bDatum)
    datums.add(bountyDatum)

    console.log("datum", bDatum)
    console.log("utxos", utxos)

    console.log("Creating output. Lovelace:", lovelace, "Gimbals:", gimbals, "Access Token:", accessPolicyID, accessTokenHex)
    console.log("Contrib Address:", contributorAddress, "Bounty Token Unit:", bountyTokenUnit)
    outputs.add(
      createTxOutput(
        Cardano.Instance.Address.from_bech32(contributorAddress),
        assetsToValue([
          { unit: "lovelace", quantity: `${lovelace}` },
          { unit: `${bountyTokenUnit}`, quantity: `${gimbals}` },
          { unit: `${accessPolicyID}${accessTokenHex}`, quantity: "1" },
        ])
      )
    )
    const bountyUtxo = createTxUnspentOutput(Cardano.Instance.Address.from_bech32(bountyContractAddress), bUtxo)

    const iAddress = fromBech32(issuerAddress)
    console.log("TRACING", issuerAddress)

    const requiredSigners = Cardano.Instance.Ed25519KeyHashes.new();
    requiredSigners.add(iAddress.payment_cred().to_keyhash());
    txBuilder.set_required_signers(requiredSigners);

    // just logging
    console.log("txBuilder", txBuilder)

    const txHash = await finalizeTx({
      txBuilder,
      datums,
      utxos,
      outputs,
      changeAddress: iAddress,
      metadata: slug,
      scriptUtxo: bountyUtxo,
      action: DISTRIBUTE,
      plutusScripts: contractScripts(),
    })

    console.log("see?", txHash)

    return {
      txHash,
    };
  }
  catch (error) {
    console.log(error, "in distributeBounty")
    return {
      error
    }
  }


};