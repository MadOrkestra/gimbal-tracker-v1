import React, { useEffect, useState } from "react";
import { Link } from "gatsby";
import { useStoreState, useStoreActions } from "easy-peasy";
import Wallet from "../cardano/wallet";
import { fromAscii, fromHex } from "../utils/converter";
import { Flex, Center, Heading, Text, Box } from "@chakra-ui/react";
import { treasuryIssuerAddress } from "../cardano/treasury-contract";
import { getAddressKeyHash } from "../utils/factory";

const IndexPage = () => {
  const connected = useStoreState((state) => state.connection.connected);
  const [ walletFunds, setWalletFunds ] = useState(null);
  const [ walletUtxos, setWalletUtxos ] = useState(null)

  useEffect(async () => {
    if (connected) {
      await Wallet.enable();
      const amt = await Wallet.getBalance();
      setWalletFunds(amt);
      console.log(amt)
    }
  }, [])

  useEffect(async () => {
    if (connected) {
      await Wallet.enable();
      const amt = await Wallet.getUtxos();
      setWalletUtxos(amt);
      console.log(amt)
    }
  }, [])

  return (
    <>
      <title>ppbl demo</title>
      <Flex w='100%' mx='auto' direction='column' wrap='wrap' bg='gl-yellow'>
          <Box w={{ base: '90%', md: '50%', lg: '30%' }} mx='auto' my='5'>
            <Heading size='2xl' color='gl-blue' fontWeight='medium'>Gimbal Bounty Treasury and Escrow, V1</Heading>
            <Heading size='md' color='gl-blue' fontWeight='medium'>An experiment!</Heading>
          </Box>
      </Flex>
    </>
  )
}

export default IndexPage;
