import React, { useEffect, useState } from "react"
import { graphql } from "gatsby"
import { useStoreState, useStoreActions } from "easy-peasy"
import { Flex, Heading, Text, Box } from "@chakra-ui/react"
import { useUtxosFromAddress } from "../hooks/useUtxosFromAddress"
import useWallet from "../hooks/useWallet"
import { bountyContractAddress } from "../cardano/treasury-contract"
import CommittedBounty from "../components/CommittedBounty"
import Complete from "../components/Bounty/Complete"

const Distribute = () => {

  // Get connected wallet + utxos from Easy Peasy
  const { wallet } = useWallet(null)
  const connected = useStoreState((state) => state.connection.connected);
  const walletUtxos = useStoreState((state) => state.ownedUtxos.utxos)
  const setWalletUtxos = useStoreActions((actions) => actions.ownedUtxos.add)

  // Use this hook to query the Bounty Contract address
  const {
    utxos: bountyUtxos,
    getUtxos,
    loading: utxosLoading,
    error,
  } = useUtxosFromAddress();


  useEffect(async () => {
    if (connected && wallet) {
      await getUtxos({
        variables: {
          addr: bountyContractAddress,
        },
      });
      const myUtxos = await wallet.utxos;
      setWalletUtxos(myUtxos);
    }
  }, [wallet])

  useEffect(async () => {
      if (wallet){
        const myUtxos = await wallet.utxos;
        setWalletUtxos(myUtxos);
      }
  }, [bountyUtxos])

  // Map each Bounty UTXO to a consumable object that can be passed as prop to <CommittedBounty />
  // What is like this?

  console.log("BOUNTY UTXOS", bountyUtxos)

  return (
    <>
      <title>DISTRIBUTE</title>
      <Flex
        w="100%"
        mx="auto"
        direction="column"
        wrap="wrap"
        bg="gl-yellow"
        p="10"
      >
        <Box w="90%" mx="auto" my="5">
          <Heading py='5'>Issuer can unlock Bounties</Heading>
          <Text fontSize='xs'>Check Nami UTXOs loaded: {JSON.stringify(walletUtxos)}</Text>
          {/* <Text>{JSON.stringify(bountyUtxos)}</Text> */}
          {bountyUtxos?.utxos?.map(i => <CommittedBounty utxo={i} key={i.transaction.metadata[0].value} />)}
        </Box>
      </Flex>
      <Complete />
    </>
  )
}

// we can grab Bounty Amounts from frontmatter: ada, gimbals

export const query = graphql`
  query GetTitles {
    allMarkdownRemark(sort: { order: ASC, fields: [frontmatter___slug] }) {
      edges {
        node {
          frontmatter {
            title
            slug
            ada
            gimbals
          }
        }
      }
    }
  }
`

export default Distribute
