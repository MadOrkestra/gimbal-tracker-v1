module.exports = {
  siteMetadata: {
    siteUrl: "https://gimbals.workshopmaybe.com",
    title: "Gimbal Bounty Treasury + Escrow",
    description: "Mainnet v1",
  },
  plugins: [
    "gatsby-transformer-sharp",
    "@chakra-ui/gatsby-plugin",
    {
      resolve: "gatsby-plugin-apollo",
      options: {
        uri: "https://d.graphql-api.mainnet.dandelion.link/",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "bounties",
        path: `${__dirname}/src/bounties`,
      },
    },
    "gatsby-transformer-remark",
  ],
};
